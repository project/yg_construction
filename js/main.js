(function ($, Drupal) {
    $(".navbar-nav li a").click(function(event) {
      var link_to = $(this).attr('href');
      window.location.href = link_to;
     });

  'use strict';
  /* CODE GOES HERE */
         function injector(t, splitter, klass, after) {
          var text = t.text()
          , a = text.split(splitter)
          , inject = '';
          if (a.length) {
            $(a).each(function(i, item) {
              inject += '<div class="'+klass+(i+1)+'" aria-hidden="true">'+item+'</div>'+after;
            });
            t.attr('aria-label',text)
            .empty()
            .append(inject)

          }
        }
        var methods = {
         
          words : function() {
            return this.each(function() {
              injector($(this), ' ', 'word', ' ');
            });
          },
          lines : function() {
            return this.each(function() {
              var r = "eefec303079ad17405c889e092e105b0";
              injector($(this).children("br").replaceWith(r).end(), r, 'line', '');
            });

          }
        };
        $.fn.lettering = function( method ) {
          if ( method && methods[method] ) {
            return methods[ method ].apply( this, [].slice.call( arguments, 1 ));
          } else if ( method === 'letters' || ! method ) {
            return methods.init.apply( this, [].slice.call( arguments, 0 ) ); // always pass an array
          }
          $.error( 'Method ' +  method + ' does not exist on jQuery.lettering' );
          return this;
        };
      
        

  $(document).ready(function() {
  $('.loop').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        dots:true
      },
      600: {
        items: 1,
        dots:true
      },
      1000: {
        items: 1,
        loop: true,
        dots:true,
        margin: 20
      }
    }
  })

$('.our-clients-carousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        dots:true
      },
      600: {
        items: 3,
        dots:true
      },
      1000: {
        items: 5,
        loop: true,
        dots:true,
        margin: 20
      }
    }
  })

  
  $('.portfolio-popup').magnificPopup({
    type: 'image',
    removalDelay: 300,
    mainClass: 'mfp-fade',
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: false,
      duration: 300,
      easing: 'ease-in-out',
      opener: function(openerElement) {
        return openerElement.is('img') ? openerElement : openerElement.find('img');
      }
    }
});

  $('.popup-youtube').magnificPopup({
         disableOn: 700,
         type: 'iframe',
         mainClass: 'mfp-fade',
         removalDelay: 160,
         preloader: false,
         fixedContentPos: false
       });
  new WOW().init();
});

$(window).on('load', function(){
    var $container = $('.portfolioContainer');
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });
     $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
         });
         return false;
    }); 
   $('.form-select option:first').text('How we can help you?*');
});

     $("#yg-slider div.carousel-caption h2, #yg-slider .carousel-caption h3,#features h2").lettering('words').children("div").lettering();
    
  

})(jQuery, Drupal);


// $(window).on('load', function(){
//     var $container = $('.portfolioContainer');
//     $container.isotope({
//         filter: '*',
//         animationOptions: {
//             duration: 750,
//             easing: 'linear',
//             queue: false
//         }
//     }); 
//     $('.portfolioFilter a').click(function(){
//         $('.portfolioFilter .active').removeClass('active');
//         $(this).addClass('active');
 
//         var selector = $(this).attr('data-filter');
//         $container.isotope({
//             filter: selector,
//             animationOptions: {
//                 duration: 750,
//                 easing: 'linear',
//                 queue: false
//             }
//          });
//          return false;
//     }); 
// });


